<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categoria;
use Illuminate\Support\Facades\Validator;


class CategoriaController extends Controller
{
    //Accion : Un metodo del controllador,contiene el codigo a ejecutar 
    //Nombre : puede ser cualquiera 
    //recomendado el nombre en minuscula
    public function index(){

       $categorias = Categoria::paginate(5);
       
       return view("categorias.index")->with("categorias" ,$categorias);
    
    }

    public function create(){

     return view ('categorias.new');

    }


    public function store(Request $r){

        $reglas = [
        "categoria" =>["required" ,"alpha"]
        ];
        $mensajes = [
                "required" => "Campo obligatorio",
                "alpha" => "Solo letras"

        ];

        $validador = Validator::make($r->all(),$reglas,$mensajes );
           if ($validador ->fails()){
         return redirect("categorias/create")->withErrors($validador);
            
        }else{


        }

        $categoria = new Categoria();

        $categoria->name = $r->input("categoria");

        $categoria->save();
        return redirect('categorias/create')->with("mensaje" , "Categoria Guardada");

       

    }

     public function edit($category_id){
         $categoria = Categoria::find($category_id);


         return view("categorias.edit")->with("categoria",$categoria);

     

     }
     public function update($category_id){
       $categoria = Categoria::find($category_id);

       $categoria->name = $_POST["name"];

       $categoria->save();

       return redirect("categiras/edit/$category_id")->with("mensaje","categoria editada");




     }
        
}   

  